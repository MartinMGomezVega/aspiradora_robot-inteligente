// 
// Decompiled by Procyon v0.5.36
// 

package Modelos;

public class Aspiradora
{
    static final int posEsquivar = 2;
    static final int posMover = 1;
    private Cepillo[] cepillos;
    private int velocidad;
    private int bateria;
    private int posicionColumna;
    private int posicionFila;
    private boolean estaPrendida;
    
    public Aspiradora() {
        this.cepillos = new Cepillo[2];
        this.velocidad = 0;
        this.bateria = 100;
        this.posicionColumna = 0;
        this.posicionFila = 0;
        this.estaPrendida = false;
    }
    
    public Cepillo[] getCepillos() {
        return this.cepillos;
    }
    
    public void setCepillos(final Cepillo[] cepillos) {
        this.cepillos = cepillos;
    }
    
    public int getVelocidad() {
        return this.velocidad;
    }
    
    public void setVelocidad(final int velocidad) {
        this.velocidad = velocidad;
    }
    
    public int getBateria() {
        return this.bateria;
    }
    
    public void setBateria(final int bateria) {
        this.bateria = bateria;
    }
    
    public int getPosicionColumna() {
        return this.posicionColumna;
    }
    
    public void setPosicionColumna(final int posicionColumna) {
        this.posicionColumna = posicionColumna;
    }
    
    public int getPosicionFila() {
        return this.posicionFila;
    }
    
    public void setPosicionFila(final int posicionFila) {
        this.posicionFila = posicionFila;
    }
    
    public static int getPosEsquivar() {
        return 2;
    }
    
    public static int getPosMover() {
        return 1;
    }
    
    public boolean getEstaPrendida() {
        return this.estaPrendida;
    }
    
    public void setEstaPrendida(final boolean estaPrendida) {
        this.estaPrendida = estaPrendida;
    }
}