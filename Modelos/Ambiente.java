// 
// Decompiled by Procyon v0.5.36
// 

package Modelos;

public class Ambiente
{
    private Suelo[][] ambiente;
    
    public Ambiente(final int filas, final int columnas) {
        this.ambiente = new Suelo[filas][columnas];
    }
    
    public Suelo[][] getAmbiente() {
        return this.ambiente;
    }
    
    public void setAmbiente(final Suelo[][] ambiente) {
        this.ambiente = ambiente;
    }
}