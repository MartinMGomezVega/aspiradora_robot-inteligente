// 
// Decompiled by Procyon v0.5.36
// 

package Tests;

import Errores.CepillosSuciosError;
import Modelos.Aspiradora;
import Modelos.Cepillo;
import Errores.SinBateriaError;
import Servicios.Implementaciones.AspiradoraServicio;
import Modelos.Suelo;
import Servicios.Implementaciones.AmbienteServicio;
import org.junit.Test;
import org.junit.Assert;
import Modelos.Ambiente;

public class AspiradoraTest
{
    @Test
    public void inicializarAmbienteCreaSueloCorrectamente() {
        final Ambiente ambiente = new Ambiente(3, 4);
        boolean correcto = true;
        if (ambiente.getAmbiente().length != 3) {
            correcto = false;
        }
        else if (ambiente.getAmbiente()[0].length != 4) {
            correcto = false;
        }
        Assert.assertTrue(correcto);
    }
    
    @Test
    public void seInicializaAmbienteCorrectamente() {
        final AmbienteServicio ambienteServicio = new AmbienteServicio(3, 3);
        ambienteServicio.inicializarAmbiente();
        boolean inicializoCorrectamente = true;
        final Suelo[][] ambiente = ambienteServicio.getAmbiente().getAmbiente();
        for (int i = 0; i < ambiente.length; ++i) {
            for (int j = 0; j < ambiente[0].length; ++j) {
                if (ambiente[j][i] == null) {
                    inicializoCorrectamente = false;
                }
            }
        }
        Assert.assertTrue(inicializoCorrectamente);
    }
    
    @Test
    public void seAsignanBordenCorrectamenteAlInicializarAmbiente() {
        final AmbienteServicio ambienteServicio = new AmbienteServicio(3, 3);
        ambienteServicio.inicializarAmbiente();
        boolean inicializoCorrectamente = true;
        final Suelo[][] ambiente = ambienteServicio.getAmbiente().getAmbiente();
        for (int i = 0; i < ambiente.length; ++i) {
            if (ambiente[0][i].getBordes().get("arriba").equals(false) || ambiente[ambiente.length - 1][i].getBordes().get("abajo").equals(false)) {
                inicializoCorrectamente = false;
            }
        }
        for (int i = 0; i < ambiente[0].length; ++i) {
            if (ambiente[i][0].getBordes().get("izquierda").equals(false) || ambiente[i][ambiente[0].length - 1].getBordes().get("derecha").equals(false)) {
                inicializoCorrectamente = false;
            }
        }
        Assert.assertTrue(inicializoCorrectamente);
    }
    
    @Test
    public void aspiradoraEnciende() throws SinBateriaError {
        final AspiradoraServicio aspiradoraServicio = new AspiradoraServicio();
        aspiradoraServicio.encender();
        Assert.assertTrue(aspiradoraServicio.getAspiradora().getEstaPrendida());
    }
    
    @Test
    public void aspiradoraCambiaVelocidad() {
        final AspiradoraServicio aspiradoraServicio = new AspiradoraServicio();
        aspiradoraServicio.getAspiradora().setVelocidad(1);
        aspiradoraServicio.cambiarVelocidad(2);
        Assert.assertEquals(2L, (long)aspiradoraServicio.getAspiradora().getVelocidad());
    }
    
    @Test
    public void seInicializanCepillosDeLaAspiradora() {
        final AspiradoraServicio aspiradoraServicio = new AspiradoraServicio();
        aspiradoraServicio.inicializarCepillos();
        final Cepillo[] cepillos = aspiradoraServicio.getAspiradora().getCepillos();
        Assert.assertTrue(cepillos[0].getClass().equals(new Cepillo().getClass()) && cepillos[1].getClass().equals(new Cepillo().getClass()));
    }
    
    @Test
    public void cepillosSuciosNoLimpian() {
        final AspiradoraServicio aspiradoraServicio = new AspiradoraServicio();
        aspiradoraServicio.inicializarCepillos();
        final Cepillo[] cepillos = aspiradoraServicio.getAspiradora().getCepillos();
        Assert.assertTrue(cepillos[0].getClass().equals(new Cepillo().getClass()) && cepillos[1].getClass().equals(new Cepillo().getClass()));
    }
    
    @Test
    public void cepilloSeCreaCorrectamente() {
        final Cepillo cepillo = new Cepillo();
        Assert.assertTrue(cepillo.getEstaLimpio() && cepillo.getSuciedad() == 0);
    }
    
    @Test
    public void aspiradoraSeCreaCorrectamente() {
        final Aspiradora aspiradora = new Aspiradora();
        Assert.assertTrue(aspiradora.getCepillos() != null && aspiradora.getVelocidad() == 0 && aspiradora.getBateria() == 100 && aspiradora.getPosicionColumna() == 0 && aspiradora.getPosicionFila() == 0 && !aspiradora.getEstaPrendida());
    }
    
    @Test(expected = SinBateriaError.class)
    public void aspiradoraSeQuedaSinBateriaCorrectamente() throws CepillosSuciosError, SinBateriaError {
        final AspiradoraServicio aspiradoraServicio = new AspiradoraServicio();
        aspiradoraServicio.inicializarCepillos();
        while (aspiradoraServicio.getAspiradora().getBateria() > 0) {
            aspiradoraServicio.bajarBateria();
        }
        aspiradoraServicio.determinarError();
    }
    
    @Test(expected = CepillosSuciosError.class)
    public void cepillosSeEnsucianCorrectamente() throws CepillosSuciosError, SinBateriaError {
        final AspiradoraServicio aspiradoraServicio = new AspiradoraServicio();
        aspiradoraServicio.inicializarCepillos();
        aspiradoraServicio.getAspiradora().getCepillos()[0].setEstaLimpio(false);
        aspiradoraServicio.determinarError();
    }
}