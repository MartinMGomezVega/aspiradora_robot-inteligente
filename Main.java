import Errores.AspiradoraApagadaError;
import Errores.CepillosSuciosError;
import Errores.SinBateriaError;
import Servicios.Implementaciones.UsuarioServicio;

// 
// Decompiled by Procyon v0.5.36
// 

public class Main
{
    public static void main(final String[] args) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError {
        final UsuarioServicio usuarioServicio = new UsuarioServicio();
        usuarioServicio.comenzarALimpiar();
    }
}