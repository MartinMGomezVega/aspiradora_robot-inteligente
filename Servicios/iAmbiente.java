// 
// Decompiled by Procyon v0.5.36
// 

package Servicios;

import Modelos.Ambiente;
import Modelos.Suelo;

public interface iAmbiente
{
    void inicializarAmbiente();
    
    void ubicarObstaculos();
    
    void determinarBordes();
    
    Suelo[][] obtenerAmbienteSuelo();
    
    Ambiente getAmbiente();
}