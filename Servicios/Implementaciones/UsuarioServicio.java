// 
// Decompiled by Procyon v0.5.36
// 

package Servicios.Implementaciones;

import Errores.AspiradoraApagadaError;
import Errores.SinBateriaError;
import Errores.CepillosSuciosError;
import java.util.Scanner;
import Servicios.iUsuario;

public class UsuarioServicio implements iUsuario
{
    private AmbienteServicio ambienteServicio;
    private AspiradoraServicio aspiradoraServicio;
    
    public UsuarioServicio() {
        this.ambienteServicio = new AmbienteServicio(this.cargarFilas(), this.cargarColumnas());
        this.aspiradoraServicio = new AspiradoraServicio();
    }
    
    @Override
    public int cargarFilas() {
        System.out.print("Ingrese la cantidad de filas: ");
        return Integer.parseInt(this.readInput());
    }
    
    @Override
    public int cargarColumnas() {
        System.out.print("Ingrese la cantidad de columnas: ");
        return Integer.parseInt(this.readInput());
    }
    
    @Override
    public int cargarVelocidad() {
        System.out.print("Ingrese la velocidad [1/2]: ");
        return Integer.parseInt(this.readInput());
    }
    
    @Override
    public String readInput() {
        final Scanner readInput = new Scanner(System.in);
        return readInput.nextLine();
    }
    
    @Override
    public void comenzarALimpiar() throws CepillosSuciosError, SinBateriaError, AspiradoraApagadaError {
        this.aspiradoraServicio.encender();
        this.aspiradoraServicio.cambiarVelocidad(this.cargarVelocidad());
        this.aspiradoraServicio.inicializarCepillos();
        this.ambienteServicio.inicializarAmbiente();
        this.mostrarAmbiente();
        this.aspiradoraServicio.mover(this.ambienteServicio.getAmbiente());
        this.mostrarAmbiente();
    }
    
    @Override
    public void mostrarAmbiente() {
        for (int i = 0; i < this.ambienteServicio.obtenerAmbienteSuelo().length; ++i) {
            for (int j = 0; j < this.ambienteServicio.obtenerAmbienteSuelo()[0].length; ++j) {
                this.ambienteServicio.obtenerAmbienteSuelo()[i][j].mostrarSuelo();
                System.out.print("\t");
            }
            System.out.print("\n");
        }
    }
}