// 
// Decompiled by Procyon v0.5.36
// 

package Servicios;

import Modelos.Aspiradora;
import Errores.AspiradoraApagadaError;
import Errores.CepillosSuciosError;
import Errores.SinBateriaError;
import Modelos.Ambiente;

public interface iAspiradora
{
    void inicializarCepillos();
    
    void cambiarVelocidad(final int p0);
    
    boolean limpiar(final Ambiente p0);
    
    void limpiarUltimaCelda(final Ambiente p0);
    
    boolean terminoDeLimpiar(final Ambiente p0);
    
    void mover(final Ambiente p0) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError;
    
    void encender() throws SinBateriaError;
    
    void moverArriba(final Ambiente p0) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError;
    
    void moverAbajo(final Ambiente p0) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError;
    
    void finalizarLimpieza(final Ambiente p0) throws CepillosSuciosError, SinBateriaError;
    
    void determinarError() throws SinBateriaError, CepillosSuciosError;
    
    void bajarBateria();
    
    void ensuciarCepillos();
    
    boolean cepillosLimpios();
    
    Aspiradora getAspiradora();
}